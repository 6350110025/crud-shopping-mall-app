import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

class InsertProducts extends StatefulWidget {
  @override
  State<InsertProducts> createState() => _InsertProductsState();
}

class _InsertProductsState extends State<InsertProducts> {
  final _formKey = GlobalKey<FormState>();

  // Handles text
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request to create new data
  Future _createProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/insert_product.php"),
      body: {
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createProducts();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false, // set it to false
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: const Color(0xFF1E1E1E),
          child: Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.only(top: 33.0, left: 20.0, right: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: const Icon(
                        Icons.arrow_back_outlined,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      tooltip: "Back",
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  'Add Product',
                  style: TextStyle(
                      fontSize: 28, fontFamily: 'IBM', color: Colors.yellow),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 9.0, vertical: 5.0),
                // height: MediaQuery.of(context).size.height * 0.35,
                // width: MediaQuery.of(context).size.width * 1,
                height: 450,
                child: Card(
                  margin: EdgeInsets.only(top: 6),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  elevation: 8,
                  child: Form(
                    key: _formKey,
                    child: Container(
                      child: (Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20, top: 30),
                            child: Container(
                              child: TextFormField(
                                controller: nameController,
                                decoration: InputDecoration(
                                    labelText: 'Product Name',
                                    prefixIcon: Icon(Icons.shopping_bag),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(8.0)),)),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Warning! : Please enter product name.';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 15.0),
                          Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20, top: 5),
                            child: Container(
                              child: TextFormField(
                                controller: priceController,
                                decoration: InputDecoration(
                                    labelText: 'Product Price',
                                    prefixIcon: Icon(Icons.currency_bitcoin),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(8.0)),)),
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  if (value!.isEmpty ||
                                      !RegExp('[0-9]').hasMatch(value)) {
                                    return 'Warning! : Please enter product price.';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                            ),
                          ),

                          SizedBox(height: 15.0),
                          Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20, top: 5),
                            child: Container(
                              child: TextFormField(
                                controller: descriptionController,
                                decoration: InputDecoration(
                                    labelText: 'Product Description',
                                    prefixIcon: Icon(Icons.info_outlined),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(8.0)),)),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Warning! :Please enter product description.';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 30.0),
                          ElevatedButton.icon(
                            label: Text('Save'),
                            icon: Icon(Icons.save_outlined),
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0)),
                              minimumSize: Size(320, 55),
                              primary: Colors.brown,
                              onPrimary: Colors.white,
                            ),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                _onConfirm(context);
                              }
                            },
                          ),
                        ],
                      )),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
